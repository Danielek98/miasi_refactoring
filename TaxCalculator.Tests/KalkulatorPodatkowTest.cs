﻿using NUnit.Framework;

namespace TaxCalculator.Tests
{
    [TestFixture]
    public class KalkulatorPodatkowTest
    {
        private readonly KalkulatorPodatkowPodstawa _kalkulatorPodatkow = new KalkulatorPodatkowPodstawa(1000m);

        private class KalkulatorPodatkowPodstawa : KalkulatorPodatkow
        {
            private const decimal KosztUzyskaniaPrzychodu = (decimal) 111.25;
            private const decimal KwotaZmniejszeniaPodatku = (decimal) 46.33;
            private const decimal KosztUzyskaniaPrzychoduMnoznik = (decimal) 20.0/100;

            public override TypUmowy TypUmowy => 0;

            public KalkulatorPodatkowPodstawa(decimal podstawa) : base(podstawa)
            {
            }

            public override void ObliczPodatki()
            {
            }
        }

        [Test]
        public void SprawdzStaleTest()
        {
            Assert.AreEqual((decimal) 111.25, _kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual((decimal) 46.33, _kalkulatorPodatkow[SkladowaPodatku.KwotaZmniejszeniaPodatku]);
            Assert.AreEqual((decimal) 20.0/100, _kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychoduMnoznik]);
            Assert.AreEqual((decimal) 9.76/100, _kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalneMnoznik]);
            Assert.AreEqual((decimal) 1.5/100, _kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentoweMnoznik]);
            Assert.AreEqual((decimal) 2.45/100, _kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChoroboweMnoznik]);
            Assert.AreEqual((decimal) 9.0/100,
                _kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9Mnoznik]);
            Assert.AreEqual((decimal) 7.75/100,
                _kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7Mnoznik]);
            Assert.AreEqual((decimal) 18/100, _kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatekMnoznik]);
        }
    }
}