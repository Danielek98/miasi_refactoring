﻿using NUnit.Framework;

namespace TaxCalculator.Tests
{
    [TestFixture]
    public class UmowaOPraceTest
    {
        [Test]
        public void ObliczPodatkiTest()
        {
            var kalkulatorPodatkow = new UmowaOPrace(2000m);
            kalkulatorPodatkow.ObliczPodatki();

            Assert.AreEqual(2000m, kalkulatorPodatkow[SkladowaPodatku.Podstawa]);
            Assert.AreEqual(195.2m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalne]);
            Assert.AreEqual(30m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentowe]);
            Assert.AreEqual(49m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChorobowe]);
            Assert.AreEqual(1725.8m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]);
            Assert.AreEqual(155.322m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9]);
            Assert.AreEqual(133.7495m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7]);
            Assert.AreEqual(111.25m, kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual(1614.55m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowania]);
            Assert.AreEqual(1615m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]);
            Assert.AreEqual(290.7m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatek]);
            Assert.AreEqual(46.33m, kalkulatorPodatkow[SkladowaPodatku.KwotaZmniejszeniaPodatku]);
            Assert.AreEqual(244.37m, kalkulatorPodatkow[SkladowaPodatku.PodatekPotracony]);
            Assert.AreEqual(110.6205m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowy]);
            Assert.AreEqual(111m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
            Assert.AreEqual(1459.478m, kalkulatorPodatkow[SkladowaPodatku.Wynagrodzenie]);
        }

        [Test]
        public void ObliczPodatkiTest2()
        {
            var kalkulatorPodatkow = new UmowaOPrace(1000m);
            kalkulatorPodatkow.ObliczPodatki();

            Assert.AreEqual(1000m, kalkulatorPodatkow[SkladowaPodatku.Podstawa]);
            Assert.AreEqual(97.6m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalne]);
            Assert.AreEqual(15m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentowe]);
            Assert.AreEqual(24.5m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChorobowe]);
            Assert.AreEqual(862.9m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]);
            Assert.AreEqual(77.661m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9]);
            Assert.AreEqual(66.87475m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7]);
            Assert.AreEqual(111.25m, kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual(751.65m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowania]);
            Assert.AreEqual(752m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]);
            Assert.AreEqual(135.36m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatek]);
            Assert.AreEqual(46.33m, kalkulatorPodatkow[SkladowaPodatku.KwotaZmniejszeniaPodatku]);
            Assert.AreEqual(89.03m, kalkulatorPodatkow[SkladowaPodatku.PodatekPotracony]);
            Assert.AreEqual(22.15525m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowy]);
            Assert.AreEqual(22m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
            Assert.AreEqual(763.239m, kalkulatorPodatkow[SkladowaPodatku.Wynagrodzenie]);
        }

        [Test]
        public void ObliczPodatkiTest3()
        {
            var kalkulatorPodatkow = new UmowaOPrace(0m);
            kalkulatorPodatkow.ObliczPodatki();

            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.Podstawa]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalne]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentowe]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChorobowe]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7]);
            Assert.AreEqual(111.25m, kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual(-111.25m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowania]);
            Assert.AreEqual(-111m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]);
            Assert.AreEqual(-19.98m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatek]);
            Assert.AreEqual(46.33m, kalkulatorPodatkow[SkladowaPodatku.KwotaZmniejszeniaPodatku]);
            Assert.AreEqual(-66.31m, kalkulatorPodatkow[SkladowaPodatku.PodatekPotracony]);
            Assert.AreEqual(-66.31m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowy]);
            Assert.AreEqual(-66m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
            Assert.AreEqual(66m, kalkulatorPodatkow[SkladowaPodatku.Wynagrodzenie]);
        }

        [Test]
        public void ObliczPodatkiTest4()
        {
            var kalkulatorPodatkow = new UmowaOPrace(10000m);
            kalkulatorPodatkow.ObliczPodatki();

            Assert.AreEqual(10000m, kalkulatorPodatkow[SkladowaPodatku.Podstawa]);
            Assert.AreEqual(976m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalne]);
            Assert.AreEqual(150m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentowe]);
            Assert.AreEqual(245m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChorobowe]);
            Assert.AreEqual(8629m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]);
            Assert.AreEqual(776.61m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9]);
            Assert.AreEqual(668.7475m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7]);
            Assert.AreEqual(111.25m, kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual(8517.75m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowania]);
            Assert.AreEqual(8518m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]);
            Assert.AreEqual(1533.24m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatek]);
            Assert.AreEqual(46.33m, kalkulatorPodatkow[SkladowaPodatku.KwotaZmniejszeniaPodatku]);
            Assert.AreEqual(1486.91m, kalkulatorPodatkow[SkladowaPodatku.PodatekPotracony]);
            Assert.AreEqual(818.1625m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowy]);
            Assert.AreEqual(818m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
            Assert.AreEqual(7034.39m, kalkulatorPodatkow[SkladowaPodatku.Wynagrodzenie]);
        }
    }
}