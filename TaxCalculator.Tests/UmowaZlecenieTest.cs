﻿using NUnit.Framework;

namespace TaxCalculator.Tests
{
    [TestFixture]
    public class UmowaZlecenieTest
    {
        [Test]
        public void ObliczPodatkiTest()
        {
            var kalkulatorPodatkow = new UmowaZlecenie(2000m);
            kalkulatorPodatkow.ObliczPodatki();

            Assert.AreEqual(2000m, kalkulatorPodatkow[SkladowaPodatku.Podstawa]);
            Assert.AreEqual(195.2m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalne]);
            Assert.AreEqual(30m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentowe]);
            Assert.AreEqual(49m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChorobowe]);
            Assert.AreEqual(1725.8m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]);
            Assert.AreEqual(155.322m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9]);
            Assert.AreEqual(133.7495m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7]);
            Assert.AreEqual(345.16m, kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual(1380.64m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowania]);
            Assert.AreEqual(1381m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]);
            Assert.AreEqual(248.58m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatek]);
            Assert.AreEqual(248.58m, kalkulatorPodatkow[SkladowaPodatku.PodatekPotracony]);
            Assert.AreEqual(114.8305m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowy]);
            Assert.AreEqual(115m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
            Assert.AreEqual(1455.478m, kalkulatorPodatkow[SkladowaPodatku.Wynagrodzenie]);
        }

        [Test]
        public void ObliczPodatkiTest2()
        {
            var kalkulatorPodatkow = new UmowaZlecenie(1000m);
            kalkulatorPodatkow.ObliczPodatki();

            Assert.AreEqual(1000m, kalkulatorPodatkow[SkladowaPodatku.Podstawa]);
            Assert.AreEqual(97.6m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalne]);
            Assert.AreEqual(15m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentowe]);
            Assert.AreEqual(24.5m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChorobowe]);
            Assert.AreEqual(862.9m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]);
            Assert.AreEqual(77.661m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9]);
            Assert.AreEqual(66.87475m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7]);
            Assert.AreEqual(172.58m, kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual(690.32m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowania]);
            Assert.AreEqual(690m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]);
            Assert.AreEqual(124.2m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatek]);
            Assert.AreEqual(124.2m, kalkulatorPodatkow[SkladowaPodatku.PodatekPotracony]);
            Assert.AreEqual(57.32525m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowy]);
            Assert.AreEqual(57m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
            Assert.AreEqual(728.239m, kalkulatorPodatkow[SkladowaPodatku.Wynagrodzenie]);
        }

        [Test]
        public void ObliczPodatkiTest3()
        {
            var kalkulatorPodatkow = new UmowaZlecenie(0m);
            kalkulatorPodatkow.ObliczPodatki();

            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.Podstawa]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalne]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentowe]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChorobowe]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowania]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatek]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.PodatekPotracony]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowy]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
            Assert.AreEqual(0m, kalkulatorPodatkow[SkladowaPodatku.Wynagrodzenie]);
        }

        [Test]
        public void ObliczPodatkiTest4()
        {
            var kalkulatorPodatkow = new UmowaZlecenie(10000m);
            kalkulatorPodatkow.ObliczPodatki();

            Assert.AreEqual(10000m, kalkulatorPodatkow[SkladowaPodatku.Podstawa]);
            Assert.AreEqual(976m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieEmerytalne]);
            Assert.AreEqual(150m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieRentowe]);
            Assert.AreEqual(245m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieChorobowe]);
            Assert.AreEqual(8629m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]);
            Assert.AreEqual(776.61m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9]);
            Assert.AreEqual(668.7475m, kalkulatorPodatkow[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7]);
            Assert.AreEqual(1725.8m, kalkulatorPodatkow[SkladowaPodatku.KosztUzyskaniaPrzychodu]);
            Assert.AreEqual(6903.2m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowania]);
            Assert.AreEqual(6903m, kalkulatorPodatkow[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]);
            Assert.AreEqual(1242.54m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaNaPodatek]);
            Assert.AreEqual(1242.54m, kalkulatorPodatkow[SkladowaPodatku.PodatekPotracony]);
            Assert.AreEqual(573.7925m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowy]);
            Assert.AreEqual(574m, kalkulatorPodatkow[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
            Assert.AreEqual(7278.39m, kalkulatorPodatkow[SkladowaPodatku.Wynagrodzenie]);
        }
    }
}