﻿using System;
using System.IO;

namespace TaxCalculator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ObliczPodatki(Console.Out, Console.Error, Console.In);
        }

        private static void ObliczPodatki(TextWriter streamWriter, TextWriter errorStreamWriter, TextReader streamReader)
        {
            decimal podstawa;
            char umowa;

            try
            {
                streamWriter.WriteLine("Podaj kwotę dochodu: ");
                podstawa = decimal.Parse(streamReader.ReadLine());

                streamWriter.Write("Typ umowy: (P)raca, (Z)lecenie: ");
                umowa = streamReader.ReadLine()[0];
            }
            catch (Exception ex)
            {
                streamWriter.WriteLine("Błędna kwota");
                errorStreamWriter.WriteLine(ex);
                return;
            }


            KalkulatorPodatkow kalkulatorPodatkow;

            switch (umowa)
            {
                case 'P':
                    kalkulatorPodatkow = new UmowaOPrace(podstawa);
                    break;
                case 'Z':
                    kalkulatorPodatkow = new UmowaZlecenie(podstawa);
                    break;
                default:
                    streamWriter.WriteLine("Nieznany typ umowy!");
                    return;
            }

            kalkulatorPodatkow.ObliczPodatki();
            kalkulatorPodatkow.Print(streamWriter);

            streamReader.Read();
        }
    }
}