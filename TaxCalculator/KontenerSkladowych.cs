﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TaxCalculator
{
    public class KontenerSkladowych : IEnumerable<KeyValuePair<SkladowaPodatku, decimal>>
    {
        private readonly Dictionary<SkladowaPodatku, decimal> _slownik = new Dictionary<SkladowaPodatku, decimal>();

        public IEnumerator<KeyValuePair<SkladowaPodatku, decimal>> GetEnumerator()
        {
            return _slownik.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public decimal PobierzSkladowa(SkladowaPodatku skladowa)
        {
            return _slownik[skladowa];
        }

        public void DodajSkladowa(SkladowaPodatku skladowa, decimal kwota)
        {
            _slownik[skladowa] = kwota;

            switch (skladowa)
            {
                case SkladowaPodatku.PodstawaOpodatkowania:
                    _slownik[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona] = Math.Round(kwota,
                        MidpointRounding.AwayFromZero);
                    break;
                case SkladowaPodatku.ZaliczkaUrzadSkarbowy:
                    _slownik[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona] = Math.Round(kwota,
                        MidpointRounding.AwayFromZero);
                    break;
            }
        }
    }
}