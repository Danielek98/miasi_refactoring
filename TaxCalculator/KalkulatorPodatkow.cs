﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace TaxCalculator
{
    public abstract class KalkulatorPodatkow : IEnumerable<KeyValuePair<SkladowaPodatku, decimal>>
    {
        protected const decimal UbezpieczenieEmerytalneMnoznik = (decimal) 9.76/100;
        protected const decimal UbezpieczenieRentoweMnoznik = (decimal) 1.5/100;
        protected const decimal UbezpieczenieChoroboweMnoznik = (decimal) 2.45/100;
        protected const decimal UbezpieczenieZdrowotneSkladka9Mnoznik = (decimal) 9.0/100;
        protected const decimal UbezpieczenieZdrowotneSkladka7Mnoznik = (decimal) 7.75/100;
        protected const decimal ZaliczkaNaPodatekMnoznik = (decimal) 18/100;

        private KontenerSkladowych KontenerSkladowych { get; }

        public decimal this[SkladowaPodatku skladowa]
        {
            get { return KontenerSkladowych.PobierzSkladowa(skladowa); }
            protected set { KontenerSkladowych.DodajSkladowa(skladowa, value); }
        }

        public abstract TypUmowy TypUmowy { get; }

        protected KalkulatorPodatkow(decimal podstawa)
        {
            KontenerSkladowych = new KontenerSkladowych();
            this[SkladowaPodatku.Podstawa] = podstawa;

            foreach (var field in GetType()
                .GetFields(BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.FlattenHierarchy))
            {
                SkladowaPodatku skladowa;
                if (Enum.TryParse(field.Name, true, out skladowa))
                    this[skladowa] = (decimal) field.GetValue(null);
            }
        }

        public IEnumerator<KeyValuePair<SkladowaPodatku, decimal>> GetEnumerator()
        {
            return KontenerSkladowych.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public abstract void ObliczPodatki();

        public virtual void Print(TextWriter writer)
        {
            foreach (var skladowa in this)
                writer.WriteLine($"{skladowa.Key}: {skladowa.Value}");
        }

        protected void ObliczUbezpieczenia()
        {
            this[SkladowaPodatku.UbezpieczenieEmerytalne] = this[SkladowaPodatku.Podstawa]*
                                                            this[SkladowaPodatku.UbezpieczenieEmerytalneMnoznik];

            this[SkladowaPodatku.UbezpieczenieRentowe] = this[SkladowaPodatku.Podstawa]*
                                                         this[SkladowaPodatku.UbezpieczenieRentoweMnoznik];

            this[SkladowaPodatku.UbezpieczenieChorobowe] = this[SkladowaPodatku.Podstawa]*
                                                           this[SkladowaPodatku.UbezpieczenieChoroboweMnoznik];

            this[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa] = this[SkladowaPodatku.Podstawa] -
                                                                   this[SkladowaPodatku.UbezpieczenieEmerytalne] -
                                                                   this[SkladowaPodatku.UbezpieczenieRentowe] -
                                                                   this[SkladowaPodatku.UbezpieczenieChorobowe];

            this[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9] = this[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]*
                                                                   this[
                                                                       SkladowaPodatku
                                                                           .UbezpieczenieZdrowotneSkladka9Mnoznik];

            this[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7] = this[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]*
                                                                   this[
                                                                       SkladowaPodatku
                                                                           .UbezpieczenieZdrowotneSkladka7Mnoznik];
        }

        protected void ObliczPodstaweIZaliczkePodatku()
        {
            this[SkladowaPodatku.PodstawaOpodatkowania] = this[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa] -
                                                          this[SkladowaPodatku.KosztUzyskaniaPrzychodu];

            this[SkladowaPodatku.ZaliczkaNaPodatek] = this[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona]*
                                                      this[SkladowaPodatku.ZaliczkaNaPodatekMnoznik];
        }

        protected void ObliczWynagrodzenie()
        {
            this[SkladowaPodatku.Wynagrodzenie] = this[SkladowaPodatku.Podstawa] -
                                                  (this[SkladowaPodatku.UbezpieczenieEmerytalne] +
                                                   this[SkladowaPodatku.UbezpieczenieRentowe] +
                                                   this[SkladowaPodatku.UbezpieczenieChorobowe] +
                                                   this[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9] +
                                                   this[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona]);
        }
    }
}