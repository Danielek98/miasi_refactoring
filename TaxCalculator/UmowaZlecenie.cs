﻿using System.IO;

namespace TaxCalculator
{
    public class UmowaZlecenie : KalkulatorPodatkow
    {
        private const decimal KosztUzyskaniaPrzychoduMnoznik = (decimal) 20.0/100;
        public override TypUmowy TypUmowy => TypUmowy.UmowaZlecenie;

        public UmowaZlecenie(decimal podstawa) : base(podstawa)
        {
        }

        public override void ObliczPodatki()
        {
            ObliczUbezpieczenia();

            this[SkladowaPodatku.KosztUzyskaniaPrzychodu] = this[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa]*
                                                            this[SkladowaPodatku.KosztUzyskaniaPrzychoduMnoznik];

            ObliczPodstaweIZaliczkePodatku();

            this[SkladowaPodatku.PodatekPotracony] = this[SkladowaPodatku.ZaliczkaNaPodatek];

            this[SkladowaPodatku.ZaliczkaUrzadSkarbowy] = this[SkladowaPodatku.ZaliczkaNaPodatek] -
                                                          this[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7];

            ObliczWynagrodzenie();
        }

        public override void Print(TextWriter writer)
        {
            writer.WriteLine("UMOWA ZLECENIE");
            writer.WriteLine($"Podstawa wymiaru składek {this[SkladowaPodatku.Podstawa].ToString("#.##")}");
            writer.WriteLine($"Składka na ubezpieczenie emerytalne {this[SkladowaPodatku.UbezpieczenieEmerytalne].ToString("#.##")}");
            writer.WriteLine($"Składka na ubezpieczenie rentowe {this[SkladowaPodatku.UbezpieczenieRentowe].ToString("#.##")}");
            writer.WriteLine($"Składka na ubezpieczenie chorobowe {this[SkladowaPodatku.UbezpieczenieChorobowe].ToString("#.##")}");
            writer.WriteLine(
                $"Podstawa wymiaru składki na ubezpieczenie zdrowotne {this[SkladowaPodatku.UbezpieczenieZdrowotnePodstawa].ToString("#.##")}");
            writer.WriteLine(
                $"Składka na ubezpieczenie zdrowotne: 9% = {this[SkladowaPodatku.UbezpieczenieZdrowotneSkladka9].ToString("#.##")} " +
                $"7,75% = {this[SkladowaPodatku.UbezpieczenieZdrowotneSkladka7].ToString("#.##")}");
            writer.WriteLine(
                $"Koszt uzyskania przychodzu w stałej wysokości {this[SkladowaPodatku.KosztUzyskaniaPrzychodu].ToString("#.##")}");
            writer.WriteLine($"Podstawa opodatkowania {this[SkladowaPodatku.PodstawaOpodatkowania].ToString("#.##")} " +
                             $"zaokrąglona {this[SkladowaPodatku.PodstawaOpodatkowaniaZaokraglona].ToString("#")}");
            writer.WriteLine($"Zaliczka na podatek dochodowy: 18% =  {this[SkladowaPodatku.ZaliczkaNaPodatek].ToString("#.##")}");
            writer.WriteLine($"Podatek potrącony =  {this[SkladowaPodatku.PodatekPotracony].ToString("#.##")}");
            writer.WriteLine($"Zaliczka do urzędu skarbowego =  {this[SkladowaPodatku.ZaliczkaUrzadSkarbowy].ToString("#.##")} " +
                             $"po zaokrągleniu = {this[SkladowaPodatku.ZaliczkaUrzadSkarbowyZaokroglona].ToString("#")}");
            writer.WriteLine(
                $"Pracownik otrzyma wynagrodzenie netto w wysokości = {this[SkladowaPodatku.Wynagrodzenie].ToString("#.##")}");
        }
    }
}