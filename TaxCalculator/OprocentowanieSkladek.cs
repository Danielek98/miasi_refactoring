﻿namespace TaxCalculator
{
    public static class OprocentowanieSkladek
    {
        public const decimal Emerytalna = 0.0976m;
        public const decimal Rentowa = 0.015m;
        public const decimal UbezpieczenieChorobowe = 0.0245m;
        public const decimal UbezpieczenieZdrowotne = 0.0775m;
        public const decimal Zdrowotna = 0.09m;
        public const decimal ZaliczkaNaPodatek = 0.18m;
    }
}
